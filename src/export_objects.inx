<?xml version="1.0" encoding="UTF-8"?>
<inkscape-extension xmlns="http://www.inkscape.org/namespace/inkscape/extension">
  <_name>Objects to Files</_name>
  <id>su-v/org.inkscape.effect.export.objects</id>

  <dependency type="executable" location="extensions">export_objects.py</dependency>
  <dependency type="executable" location="extensions">inkex.py</dependency>

  <param name="tab" type="notebook">
    <page name="options" _gui-text="Export">
      <param name="type_header" type="description" appearance="header">Export Format and Type</param>
      <param name="export_format" type="enum" _gui-text="Format:">
        <item value="svg">SVG</item>
        <item value="png">PNG</item>
        <item value="gif">GIF</item>
        <item value="jpg">JPEG</item>
        <item value="pdf">PDF</item>
      </param>
      <param name="export_type" type="enum" _gui-text="Type:">
        <item value="files">Individual files</item>
        <item value="tar">Tarball</item>
        <item value="zip">ZIP Archive</item>
      </param>
      <param name="scope_header" type="description" appearance="header" _gui-description="The active tab below will be used for current export">Export Scope</param>
<!--
      <param name="scope_desc" type="description">The active tab below will be used for current export:</param>
-->
      <param name="nb_scope" type="notebook">
        <page name="selection" _gui-text="Selection">
          <param name="selection_desc" type="description" _gui-description="Export the current selection (multipe objects, or the members of a single selected group) to individual files. Optionally include everything else (not part of the selection) in the template (always visible).">See tooltip for description.</param>
          <param name="selection_spacer" type="description">&#160;</param>
          <param name="selection_other" type="boolean" _gui-text="Include unselected objects in template">false</param>
          <param name="selection_fitpage" type="boolean" _gui-text="Fit page to object" _gui-description="Checking this option is not compatible with export for animation.">false</param>
        </page>
        <page name="layer" _gui-text="Layers">
          <param name="layer_other" type="boolean" _gui-text="Include common layers (see below) in template">false</param>
          <param name="layer_list" type="string" _gui-text="Common layers:" _gui-description="Comma-separated list of layers to be included in all exported files.">Background,Overlay</param>
          <param name="layer_visible" type="boolean" _gui-text="Export visible layers only">false</param>
          <param name="layer_reset" type="boolean" _gui-text="Reset layer opacity to 100% for export">true</param>
          <param name="layer_fitpage" type="boolean" _gui-text="Fit page to layer" _gui-description="Checking this option is not compatible with export for animation.">false</param>
        </page>
        <page name="sublayer" _gui-text="Sublayers">
          <param name="sublayer_other" type="boolean" _gui-text="Include common top-level layers in template">false</param>
          <param name="sublayer_list" type="string" _gui-text="Common layers:" _gui-description="Comma-separated list of top-level layers to be included in all exported files.">Background,Overlay</param>
          <param name="sublayer_visible" type="boolean" _gui-text="Export visible sublayers only">false</param>
          <param name="sublayer_reset" type="boolean" _gui-text="Reset sublayer opacity to 100% for export">true</param>
          <param name="sublayer_scope" type="enum" _gui-text="Scope:" _gui-description="Sublayers from top-level layers excluding any common ones defined above">
            <item value="sublayer_current">Sublayers of current top-level layer only</item>
            <item value="sublayer_all">Sublayers of all top-level layers</item>
            <item value="sublayer_layer_visible">Sublayers of visible top-level layers only</item>
          </param>
          <param name="sublayer_level" type="int" min="1" max="10" _gui-text="Sublayer level to export:">1</param>
          <param name="sublayer_fitpage" type="boolean" _gui-text="Fit page to sublayer" _gui-description="Checking this option is not compatible with export for animation.">false</param>
        </page>
      </param>
      <param name="filepath_header" type="description" appearance="header">Export Path and Filename</param>
      <param name="export_path" type="string" _gui-text="Export directory:" _gui-description="A relative path (export directory) is relative to the user's home directory. The directory needs to exist (the extension does not create it if not found).">splitgroup</param>
      <param name="basename" type="string" _gui-text="Basename:" _gui-description="Used as name for exported files, with counter of each step appended">step</param>
      <param name="counter_zfill" type="int" min="1" max="6" _gui-text="Number of digits for counter:" _gui-description="The counter is padded with leading zeros; the initial zfill value is increased automatically if too low for the total count of objects to be exported">5</param>
      <param name="counter_start" type="int" min="0" max="99999" _gui-text="Start value for counter:">1</param>
    </page>
    <page name="document" _gui-text="Content">
      <param name="document_header" type="description" appearance="header">Document options</param>
      <param name="with_defs" type="boolean" _gui-text="Include defs in template">false</param>
      <param name="reverse" type="boolean" _gui-text="Reverse z-order for export">false</param>
      <param name="incremental" type="boolean" _gui-text="Incremental export">false</param>
      <param name="background" type="enum" _gui-text="Document background:" _gui-description="This option will set the alpha value of the document background color accordingly.">
        <item value="default">Default</item>
        <item value="0">Default color, alpha=0</item>
        <item value="1">Default color, alpha=1</item>
        <item value="custom">Custom (see below)</item>
      </param>
      <param name="color_header" type="description" appearance="header">Custom Background Color</param>
      <param name="background_color" type="color" gui-text="Background color" _gui-description="Override the document property 'Background color' for SVG, PNG, GIF and JPEG export with custom color and alpha (transparency).">-256</param>
      <param name="fitpage_header" type="description" appearance="header">Margins</param>
      <param name="fitpage_margin" type="float" min="-999" max="999" precision="2" _gui-text="Margin for 'Fit page' option:">0</param>
      <param name="fitpage_margin_unit" type="enum" _gui-text="Units for page margin:">
        <item value="px">px</item>
        <item value="pt">pt</item>
        <item value="mm">mm</item>
        <item value="cm">cm</item>
      </param>
    </page>
    <page name="file_formats" _gui-text="File Formats">
      <param name="svg_header" type="description" appearance="header">SVG</param>
      <param name="svg_plain" type="boolean" _gui-text="Save as Plain SVG">false</param>
      <param name="svg_text_outline" type="boolean" _gui-text="Outline text in Plain SVG" _gui-description="Convert text to path (only for Plain SVG)">false</param>
      <param name="png_header" type="description" appearance="header">PNG</param>
      <param name="png_resolution" type="int" min="1" max="1000" _gui-text="Export resolution:" _gui-description="GIF, JPEG are based on PNG export, and will have the same pixel dimensions as the exported PNGs.">96</param>
      <param name="gif_header" type="description" appearance="header">GIF</param>
      <param name="gif_interlace" type="boolean" _gui-text="Interlace (Line)">false</param>
      <param name="jpg_header" type="description" appearance="header">JPEG</param>
      <param name="jpg_interlace" type="boolean" _gui-text="Interlace (Line)">false</param>
      <param name="jpg_quality" type="int" min="0" max="100" _gui-text="Quality (%):">90</param>
      <param name="pdf_header" type="description" appearance="header">PDF</param>
      <param name="pdf_text_outline" type="boolean" _gui-text="Outline text" _gui-description="Convert text to path via cairo">false</param>
      <param name="pdf_filter_raster" type="boolean" _gui-text="Rasterize filter effects">true</param>
      <param name="pdf_resolution" type="int" min="1" max="1000" _gui-text="PDF filter effects resolution:" _gui-description="Resolution used for rasterizing SVG filter effects">96</param>
    </page>
    <page name="animation" _gui-text="Animation">
      <param name="anim_header" type="description" appearance="header">Animated GIF Options</param>
      <param name="anim_desc" type="description" _gui-description="Optionally create an animated GIF based on the exported page-sized bitmap images. The result will be a simple not-optimized coalesced animation (an image sequence like a 'film-strip').">See tooltip for description.</param>
      <param name="anim_spacer" type="description">&#160;</param>
      <param name="anim_gif" type="boolean" _gui-text="Create animated GIF from exported bitmaps" _gui-description="When exporting to PNG, GIF or JPEG file format, an animated GIF can be created automatically from the exported bitmap images.">false</param>
      <param name="anim_gif_filename" type="string" _gui-text="Filename for animated GIF:" _gui-description="Leave empty to use same basename"></param>
      <param name="anim_gif_delay" type="int" min="20" max="100000" _gui-text="Frame delay (milliseconds):" _gui-description="Time delay in 1/1000th of a second">20</param>
      <param name="anim_gif_loop" type="int" min="0" max="100000" _gui-text="Loop count (0 for infinite):" _gui-description="Zero is infinite loop.">1</param>
      <param name="anim_gif_dispose" type="enum" _gui-text="Image disposal:">
        <item value="3">Previous</item>
        <item value="2">Background</item>
        <item value="1">None</item>
        <item value="0">Undefined</item>
      </param>
      <param name="anim_gif_optim" type="boolean" _gui-text="Optimize the animated GIF with ImageMagick">false</param>
<!--
      <param name="anim_gif_cmd" type="boolean" _gui-text="NYI: Save command to create animated GIF as script">false</param>
-->
    </page>
    <page name="help" _gui-text="Help">
      <param name="help_header" type="description" appearance="header">Description</param>
      <param name="help_desc" type="description" _gui-description="This extension exports each selected object or all objects inside a selected group or individual layers to separate SVG files. Optionally the exported files inlcude all other drawing content not part of the current selection. Supported export types are individual files, tarball or ZIP archive. The extension can be useful for exporting the result of an interpolation to individual animation frames.">See tooltip.</param>
      <param name="issues_header" type="description" appearance="header">Known Limitations</param>
      <param name="issues_desc1" type="description" _gui-description="Shared resources are not handled automatically: the user has to decide whether or not to include the defs section in the template file.">* Shared resources</param>
      <param name="issues_desc2" type="description" _gui-description="References across layers (e.g. a clone with the original object on a different layer) are not resolved before exporting layers to individual files.">* References across layers</param>
    </page>
  </param>

  <effect needs-document="true" needs-live-preview="false">
    <object-type>all</object-type>
    <menu-tip>Export objects or layers to individual files.</menu-tip>
    <effects-menu>
      <submenu _name="Export"/>
    </effects-menu>
  </effect>

  <script>
    <command reldir="extensions" interpreter="python">export_objects.py</command>
  </script>

</inkscape-extension>
